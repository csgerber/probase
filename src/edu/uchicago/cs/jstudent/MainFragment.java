package edu.uchicago.cs.jstudent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import edu.uchicago.jstudent.probase.R;

public class MainFragment extends Fragment {

    EditText mHelloField;
    Button mHelloButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, parent, false);

        mHelloField =  (EditText) v.findViewById(R.id.hello_edit_text);

        mHelloButton = (Button) v.findViewById(R.id.hello_button);
        mHelloButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(),"hello " + mHelloField.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        
        return v; 
    }
}
